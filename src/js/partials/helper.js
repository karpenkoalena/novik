$(document).ready(function () {

});

let burger = document.querySelector('.menu--burger');
let nav = document.querySelector('.navigation');
let navli = document.querySelectorAll('.navagation--item a');

burger.onclick = function (){
    burger.classList.toggle('open')
    nav.classList.toggle('open-nav')
    }

for ( let i = 0; i < navli.length; i++ ) {
    navli[i].onclick = function () {
        burger.classList.remove('open')
        nav.classList.remove('open-nav')
    }
}
const smoothLinks = document.querySelectorAll('a[href^="#"]');
for (let smoothLink of smoothLinks) {
    smoothLink.addEventListener('click', function (e) {
        e.preventDefault();
        const id = smoothLink.getAttribute('href');

        document.querySelector(id).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
};